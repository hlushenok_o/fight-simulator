class Fighter {
  constructor(fighter, status) {
    this._fighter = fighter;
    this._fighter.status = status;
  }

  getHitPower() {
    let criticalHitChance = Math.random() * 2 + 1;
    let power = this._fighter.attack * criticalHitChance;
    return power;
  }
  getBlockPower() {
    let dodgeChance = Math.floor(Math.random() * 2) + 1;
    let power = this._fighter.defense * dodgeChance;
    return power;
  }
}

export default Fighter;
