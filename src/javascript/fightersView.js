import View from "./view";
import Fighter from "./fighter";
import FighterView from "./fighterView";
import FightView from "./fightView";
import { fighterService } from "./services/fightersService";

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();
  botDetailsArr = new Array();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({
      tagName: "div",
      className: "fighters"
    });
    this.element.append(...fighterElements);
  }

  async getFighterDetail(fighter) {
    const fighterDetails = await fighterService.getFighterDetails(fighter._id);
    return fighterDetails;
  }

  createDetailsModal(fighterInfo) {
    const modalWrapper = this.createElement({
      tagName: "div",
      className: "fighter__detail-wrapper"
    });
    const detailModal = this.createElement({
      tagName: "div",
      className: "fighter__detail"
    });
    const closeIcon = this.createElement({
      tagName: "img",
      className: `fighter__detail-close`,
      attributes: { src: "./resources/close_icon.png" }
    });
    detailModal.append(closeIcon);

    closeIcon.addEventListener("click", () => modalWrapper.remove());

    const { _id, ...details } = fighterInfo;

    Object.keys(details).forEach(key => {
      const detailRow = this.createElement({
        tagName: "div",
        className: `fighter__detail-row`
      });
      if (key === "source") {
        const attributes = { src: details[key] };
        const fighterImage = this.createElement({
          tagName: "img",
          className: `fighter__detail-image`,
          attributes
        });
        detailModal.append(fighterImage);
      } else {
        const detailPropName = this.createElement({
          tagName: "span",
          className: `fighter__detail-${key}`
        });
        detailPropName.innerText = `${key}: `;
        let detailPropVal = this.createElement({
          tagName: "span",
          className: `fighter__detail-value`
        });
        let detailEdit = this.createElement({
          tagName: "sup",
          className: `fighter__detail-value--edit`,
          attributes: {
            "data-key": key
          }
        });
        detailEdit.innerText = "EDIT";
        detailPropVal.innerText = `${details[key]}`;
        detailRow.append(detailPropName, detailPropVal, detailEdit);
        detailModal.append(detailRow);
      }
    });

    modalWrapper.append(detailModal);

    return modalWrapper;
  }

  showModal(fighterInfo) {
    const detailModal = this.createDetailsModal(fighterInfo);
    const root = document.querySelector("#root");
    const modal = document.querySelector(".fighter__detail-wrapper");
    if (modal) {
      modal.remove();
      root.append(detailModal);
    } else {
      root.append(detailModal);
    }
    detailModal.style.display = "block";
  }

  showStartButton() {
    const startFightButton = this.createElement({
      tagName: "button",
      className: `start-fight`
    });
    startFightButton.innerText = "Почати бій!";
    const btn = document.querySelector(".start-fight");
    const root = document.querySelector("#root");
    if (btn) {
      btn.remove();
      root.append(startFightButton);
    } else {
      root.append(startFightButton);
    }
    return startFightButton;
  }

  async handleFighterClick(event, fighter) {
    const allFighters = document.querySelectorAll(".fighter");
    let activeFighter = event.target.closest(".fighter");
    const details = await this.getFighterDetail(fighter);
    allFighters.forEach(f => {
      f.classList.remove("active");
    });
    activeFighter.classList.add("active");
    this.fightersDetailsMap.set(fighter._id, details);
    this.botDetailsArr.push(
      await this.getFighterDetail({
        _id: `${Math.floor(Math.random() * 6) + 1}`
      })
    );

    const fighterInfo = this.fightersDetailsMap.get(fighter._id);
    const botInfo = this.botDetailsArr[0];
    this.showModal(fighterInfo);
    this.showStartButton().addEventListener("click", () => {
      const player = new Fighter(fighterInfo, "player");
      const bot = new Fighter(botInfo, "bot");
      new FightView(player, bot);
    });
  }
}

export default FightersView;
