import View from "./view";
import Fighter from "./fighter";
import FighterView from "./fighterView";
import FightersView from "./fightersView";

class FightView extends View {
  constructor(player, bot) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFight(player, bot);
  }

  createFight(player, bot) {
    let elements = document.querySelectorAll("body #root *");
    let root = document.querySelector("#root");
    root.classList.add("fight");
    elements.forEach(e => {
      e.remove();
    });

    const playerView = new FighterView(
      player._fighter,
      this.handleClick,
      "player"
    );
    const botView = new FighterView(bot._fighter, this.handleClick, "bot");

    root.append(playerView.element);
    root.append(botView.element);

    this.fight(player, bot);
  }

  createHealthBar(fighter) {
    const status = this.createElement({
      tagName: "div",
      className: `progress-status`
    });
    const progressBar = this.createElement({
      tagName: "div",
      className: `progress-bar`
    });
    status.classList.add(fighter._fighter.status);
    status.append(progressBar);
    root.append(status);

    return status;
  }

  hitAnimate() {
    let playerElement = document.querySelector(".fighterplayer");
    let botElement = document.querySelector(".fighterbot");
    playerElement.classList.add("hitplayerAnim");
    botElement.classList.add("hitbotAnim");
    setTimeout(() => {
      playerElement.classList.remove("hitplayerAnim");
      botElement.classList.remove("hitbotAnim");
    }, 1000);
  }

  createHitButtom() {
    const a = document.createElement("button");
    a.classList.add("hit");
    a.innerText = "Розмінятися ударами";
    let root = document.querySelector("body #root");
    root.append(a);
    return a;
  }
  createRestartButtom() {
    const a = document.createElement("button");
    a.classList.add("restart");
    a.innerText = "Спробувати ще";
    let root = document.querySelector("body #root");
    root.append(a);
    a.addEventListener("click", () => window.location.reload(false));
    return a;
  }

  fight(player, bot) {
    let playerElement = document.querySelector(".fighterplayer");
    let botElement = document.querySelector(".fighterbot");
    let hitButton = this.createHitButtom();
    let botHealthStart = bot._fighter.health;
    let playerHealthStart = player._fighter.health;
    let playerHealth = this.createHealthBar(player);
    let botHealth = this.createHealthBar(bot);
    hitButton.addEventListener("click", () => {
      this.hitAnimate();
      let playerDamage = player.getHitPower() - bot.getBlockPower();
      let botDamage = bot.getHitPower() - player.getBlockPower();
      if (playerDamage < 0) playerDamage = 0;
      if (botDamage < 0) botDamage = 0;

      player._fighter.health = player._fighter.health - botDamage;
      bot._fighter.health = bot._fighter.health - playerDamage;

      let playerDiff = (player._fighter.health / playerHealthStart) * 100;
      playerHealth.firstChild.style.width = `${playerDiff}%`;

      let botDiff = (bot._fighter.health / botHealthStart) * 100;
      botHealth.firstChild.style.width = `${botDiff}%`;

      if (player._fighter.health <= 0) {
        alert("Ти програв");
        playerElement.classList.add("dead");
        this.createRestartButtom();
        hitButton.remove();
      } else if (bot._fighter.health <= 0) {
        alert("Ти виграв");
        botElement.classList.add("dead");
        this.createRestartButtom();
        hitButton.remove();
      }
    });
  }

  handleFighterClick(event, fighter) {
    console.log(_fighter);
  }
}

export default FightView;
